DESCRIPTION
===========
git-export-filter allows one to filter a git fast-export data stream optionally
rewriting committer/author and/or branch/tag names.

BUILDING
========
Simply typing "make" should suffice to build the git-export-filter executable.

LICENSE
=======
See the LICENSE.txt file, license is GPLv2 (or later).

HELP
====
See the git-export-help.txt file or run git-export-filter -v -h.
