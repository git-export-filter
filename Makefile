.PHONY : all clean

CC ?= cc
CFLAGS ?= -O2
COPTS ?= -Wall -Wextra

all : git-export-filter

clean :
	rm -rf *.dSYM *.o *.inc git-export-filter

git-export-filter.o : git-export-filter.c git-export-help.inc
	$(CC) $(CFLAGS) $(COPTS) -c -o $@ $*.c

git-export-filter : git-export-filter.o
	$(CC) $(CFLAGS) -o $@ $@.o
	case "$(CFLAGS)" in *"-g"*) if [ "`uname -s`" = "Darwin" ]; then dsymutil $@; fi; esac

git-export-help.inc : git-export-help.txt
	sed -e 's/\\/\\\\/g;s/"/\\"/g;s/^/"/;s/$$/\\n"/;' $? > $@
